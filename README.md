# Glasto Tickets Automation Help

## Introduction
This is a simple tool that will automatically try to get through and get Glasto tickets for you and your group of friends.

We all know how frustratingly shit it is to get Glasto tickets, so this tool aims to help.

You simply run the Java JAR file in Terminal with all your reg details passed in as parameters and let the tool do the rest.

It keeps constantly refreshing the holding page as soon as it gets a response that isnt from the /event/addregistrations page.

Once on the /event/addregistrations page, it automatically enters the reg details you provide and submits them.

If the page after crashes, it automatically goes back one page and resubmits the reg details.  It keeps doing this until it successfully arrives on the payment page, at which pint you can manually enter your payment details :)

Source code of the program I made can be found at https://pastebin.com/P1su2b9x (so you can see exactly what the program actually does) :D

## Setup Instructions
This tool only works for one browser at a time, you need to choose whether you want to run it on Chrome or Firefox!

If you want to run it on Chrome...
 - download the glasto-chrome JAR file from <https://drive.google.com/file/d/1DWlNYtV86cwb3hVLJ3PPwyXOXe2ewx_f/view?usp=sharing>

If you want to run it on Firefox...
 - download the glasto-firefox JAR file from <https://drive.google.com/file/d/1_TwWZdwmpFbFP_NMFkj-MGd4KYrgYCYf/view?usp=sharing>
 
 Obviously make sure you have Chrome and/or Firefox installed on your Mac / PC.
 
 ### Setting up JAVA
 For the tool to work, you need JAVA installed on your computer. Java is a general-purpose computer-programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible.
 
 #### Java JDK
 The Java Development Kit is an implementation of either one of the Java Platform, Standard Edition, Java Platform, Enterprise Edition, or Java Platform, Micro Edition platforms released by Oracle.
 
 1. Download the Java JDK appropriate for your Operating System / Architecture from <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
 2. Follow the wizard and complete the installation of the Java JDK
 
 #### Setting Up Java Environment Variables
 An environment variable is a dynamic-named value that can affect the way running processes will behave on a computer.  Because every developer on a team will have a slightly different Development environment on there local machine, using environment variables is good practice to help easily find and use needed processes and/or paths in an environment.
 
 This needs to be done so we can run JAVA commands easily from the Terminal / Command Line.
 
 ##### MacOS
 1. Open up Terminal and enter the following... `nano ~/.bash_profile`
 2. Add the following lines to your .bash_profile file within Terminal to set the $JAVA_HOME environment variable…
 `export JAVA_HOME=$(/usr/libexec/java_home)`
 and
 `export PATH=$JAVA_HOME/bin:$PATH`
 3. Save the .bash_profile and exit the nano editor by pressing (in order, one at a time)...
 `Ctrl + O`,
 `[Return]`, and
 `Ctrl + X`
 4. Completely quit out of Terminal and restart it to launch a fresh Terminal window with your changes applied
 5. Confirm you have correctly set the $JAVA_HOME environment variable by entering in Terminal... `java -version` and pressing Enter
 
 ##### Windows
 1. Click ‘Start’ and search for “System”
 2. Go to ‘Advanced System Settings’ –> ‘Advanced’ –> ‘Environment Variables’
 3. Under the ‘System Variables’ section, click ‘New’ and then add and set the following new System Variable…
    1. Variable Name: `JAVA_HOME`
    2. Variable Value: `C:\Program Files\jdk1.#.#_###` (replace with directory path of where your Java JDK was installed to)
 4. Double-click on 'Path' in the System Variables pane
 5. Set and append (add to end of the text string) the following PATH environment variable...
    `C:\Program Files\jdk1.#.#_###\bin;` (replace with your JDK-root-directory/bin)
 6. Click 'OK' where applicable to apply your changes
 7. Confirm you have correctly set the %JAVA_HOME% environment variable by opening CMD Prompt and entering `java -version`
 
 ## Running the Program
 1. Copy the JAR file you downloaded to an easy location, like your Desktop
 2. Open up a fresh new Terminal window (or Command Prompt window if on Windows)
 3. Enter in the following command, replacing the values like "regOne" and "postCodeOne" etc with your own group's reg details (keep the quotes around each one) and also replacing the directory path to your own desktop path (or wherever you put the JAR file)...
 
 `java -jar /Users/username/Desktop/glasto-chrome.jar "regOne" "postCodeOne" "regTwo" "postCodeTwo" "regThree" "postCodeThree" "regFour" "postCodeFour" "regFive" "postCodeFive" "regSix" "postCodeSix"` for Chrome
 
 Or...
 
 `java -jar /Users/username/Desktop/glasto-firefox.jar "regOne" "postCodeOne" "regTwo" "postCodeTwo" "regThree" "postCodeThree" "regFour" "postCodeFour" "regFive" "postCodeFive" "regSix" "postCodeSix"` for Firefox.

To stop the process in Terminal or command prompt, press Ctrl + C